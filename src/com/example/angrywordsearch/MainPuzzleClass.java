package com.example.angrywordsearch;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Adapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


public class MainPuzzleClass extends Activity  implements OnRetrieveDataCompleted, OnSendDataCompleted {

	LoginHandler _loginHandler;

	GridView PuzzleGrid;
	GridView wordsgrid;
	int [] PosSelected = new int[20];
	String CharArray = new String();
	TextView [] Views = new TextView[20];
	int PosInArray = 0;
	String PuzzleString = "", Scores = "", Puzzles = "";
	Puzzle _puzzle;
	int shortestWord = 100, longestWord = 0;
	ProgressDialog pdialog;
	boolean TodaysPuzzleLoaded = false;
	
	double dist0;

	double distCurrent;

	float currentXPosition, currentYPosition, currentX1Position, currentPosition;
	//private static float MIN_ZOOM = -2f;
	//private static float MAX_ZOOM = 5f;
	private float scaleFactor = 1.0f;
	private ScaleGestureDetector detector;
	
	float eventX = 0;
	float eventY = 0;
	float eventX1 = 0;
	float eventY1 = 0;
	
	//Touch event related variables
	int touchState;
	final int IDLE = 0;
	final int TOUCH = 1;
	final int PINCH = 2;
	//final int DRAG = 3;
	
	enum WordStatus {NOMATCH, PARTIALMATCH, MATCH, BACKWARDMATCH, BACKWARDPARTIAL};
	
	

private static final float MIN_ZOOM = 1.0f;
private static final float MAX_ZOOM = 5.0f;

// These matrices will be used to move and zoom image
Matrix matrix = new Matrix();
Matrix savedMatrix = new Matrix();

// We can be in one of these 3 states
static final int NONE = 0;
static final int DRAG = 1;
static final int ZOOM = 2;
int mode = NONE;

// Remember some things for zooming
PointF start = new PointF();
PointF mid = new PointF();
float oldDist = 1f;
int SelectedColour = Color.YELLOW;
	
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		
		@Override
			public boolean onScale(ScaleGestureDetector detector) {
			scaleFactor *= detector.getScaleFactor();
			scaleFactor = Math.max(MIN_ZOOM, Math.min(scaleFactor, MAX_ZOOM));
			PuzzleGrid.setScaleX(scaleFactor);
			PuzzleGrid.setScaleY(scaleFactor);
			return true;
			}
		}
	
	float DragPosX = 0, DistanceDraggedX = 0, StartPosX = 0;
	float DragPosY = 0, DistanceDraggedY = 0, StartPosY = 0;
	boolean Dragging = false;
	
	OnTouchListener MyOnTouchListener = new OnTouchListener(){
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			
			float distx, disty;
			currentXPosition = event.getX();
            currentYPosition = event.getY();

            switch(event.getAction() & MotionEvent.ACTION_MASK){
            
			case MotionEvent.ACTION_DOWN:
				//A pressed gesture has started, the motion contains the initial starting location.
//				WordDisplay.setText("ACTION_DOWN"); 
				touchState = DRAG;
				StartPosX = event.getX();
				StartPosY = event.getY();
				int position = PuzzleGrid.pointToPosition((int) currentXPosition, (int) currentYPosition);
				if(position != -1){
					// Adapter adapter = PuzzleGrid.getAdapter();
					 //TextView textview = (TextView) adapter.getItem(position); //Get Text
					//String text = textview.getText().toString();
					//CheckWord(textview,position,text);
				}
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				//A non-primary pointer has gone down.
//				WordDisplay.setText("ACTION_POINTER_DOWN"); 
				touchState = PINCH;
				//Get the distance when the second pointer touch
				eventX1 = event.getX(1);
				eventY1 = event.getY(1);
				
				distx = currentXPosition - eventX1;
				disty = currentYPosition- eventY1;
				
				dist0 = Math.sqrt(distx * distx + disty * disty);
				break;
				
			case MotionEvent.ACTION_MOVE:
				//A change has happened during a press gesture (between ACTION_DOWN and ACTION_UP).
//				WordDisplay.setText("ACTION_MOVE");
				if(touchState == PINCH){						
					eventX1 = event.getX(1);
					eventY1 = event.getY(1);

					distx = currentXPosition - eventX1;
					disty = currentYPosition- eventY1;
					
					distCurrent = Math.sqrt(distx * distx + disty * disty);
				}
				else
				{
					if(touchState == DRAG)
					{
						int position2 = PuzzleGrid.pointToPosition((int) currentXPosition, (int) currentYPosition);
						if(position2 != -1){
							 Adapter adapter = PuzzleGrid.getAdapter();
							 TextView textview = (TextView) adapter.getItem(position2); //Get Text
							String text = textview.getText().toString();
							CheckWord2(position2,text);
						} 
					}
				}
				break;
				
			case MotionEvent.ACTION_UP:
				//int position2 = PuzzleGrid.pointToPosition((int) currentXPosition, (int) currentYPosition);
				//if(position2 != -1){
					IsItaWord();
				//}
				//A pressed gesture has finished.
				touchState = IDLE;
				break;
				
			case MotionEvent.ACTION_POINTER_UP:
				DistanceDraggedX = event.getX();
				DistanceDraggedY = event.getY();
				distx = StartPosX - DistanceDraggedX;
				disty = StartPosY - DistanceDraggedY;
//					 DragPosY = (gridView.getHeight());

				DragPosX -= distx;
				DragPosY -= disty;


				PuzzleGrid.setX(DragPosX);
				PuzzleGrid.setY(DragPosY);
				touchState = DRAG;
				break;
			}
            detector.onTouchEvent(event);	
//            gridWords.refreshDrawableState();
			return true;
			
		}
    };
		
    
	public String GetDate()  {
		Calendar cal=Calendar.getInstance();		
		cal.add(Calendar.DAY_OF_MONTH, 0);
		Date currentdate = new Date(cal.getTimeInMillis());
		return currentdate.toString();
	}
	
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
		    setContentView(R.layout.mainpuzzle);
		    _loginHandler = LoginParent._loginHandler;
		    detector = new ScaleGestureDetector(this, new ScaleListener());
		    Intent i= getIntent();

		    	if(i.getExtras() != null){
			    	try {
					PuzzleString = i.getStringExtra("Puzzle");
				     } catch (Exception e){}
			    	try {
				    	Scores = i.getStringExtra("Scores");
					} catch (Exception e){}
			    	try{
			    		Puzzles = i.getStringExtra("PreviousSolutions");
			    	} catch(Exception e){}
		    	}
	
			    if(PuzzleString.equals("")){ //Load New Puzzle
			        String url = "http://08309.net.dcs.hull.ac.uk/api/wordsearch/current?username=" + _loginHandler.get_uname() + "&password=" + _loginHandler.get_password();
					RetrieveResponseData retrieveResponseData = new RetrieveResponseData(this);
					retrieveResponseData.execute(url);
					
					pdialog=new ProgressDialog(this);
					pdialog.setCancelable(true);
					pdialog.setMessage("Loading ....");
					pdialog.show();
			    } else { //Load From Memory
					_puzzle = StringtoArray(PuzzleString);
					if(_puzzle.finished)
					{
						if((!GetDate().equals(_puzzle.Date)) && TodaysPuzzleLoaded == false){
					        String url = "http://08309.net.dcs.hull.ac.uk/api/wordsearch/current?username=" + _loginHandler.get_uname() + "&password=" + _loginHandler.get_password();
							RetrieveResponseData retrieveResponseData = new RetrieveResponseData(this);
							retrieveResponseData.execute(url);
							pdialog=new ProgressDialog(this);
							pdialog.setCancelable(true);
							pdialog.setMessage("Loading ....");
							pdialog.show();
						}
						DrawGrids();
					} else {
						DrawGrids();
					}

			    }

			Arrays.fill(PosSelected, -1);

		 }

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.angry_wordsearch, menu);
			
			return true;
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
		    // Handle item selection
		    switch (item.getItemId()) {
		        case R.id.action_playinfo:
					Intent nextScreen = new Intent(this,UserDetailsClass.class);
					PuzzleString = ArraytoString(_puzzle);
					nextScreen.putExtra("Puzzle", PuzzleString);
					nextScreen.putExtra("PreviousSolutions", Puzzles);
					nextScreen.putExtra("Scores", Scores);
					this.startActivity(nextScreen);
		            return true;
		            
		        case R.id.Action_PrevPuzzle:
					nextScreen = new Intent(this,PreviousPuzzlesClass.class);
					PuzzleString = ArraytoString(_puzzle);
					nextScreen.putExtra("Puzzle", PuzzleString);
					nextScreen.putExtra("PreviousSolutions", Puzzles);
					nextScreen.putExtra("Scores", Scores);
					this.startActivity(nextScreen);
					this.finish();
		            return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
		
	    protected void onStop() {
		    super.onStop();
		    outputToFileStream("end.xml");
	    }
	    
	    @Override
	    protected void onPause() {
		    super.onPause();
		    outputToFileStream("end.xml");
	    }

		private Document generateClose() {
			try {
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document document = documentBuilder.newDocument();
				
				Element rootElement = document.createElement("State");
				   document.appendChild(rootElement);

				Element State = document.createElement("PrevState");
				State.appendChild(document.createTextNode("Puzzle"));
				
			   Element UsernameElement = document.createElement("Username");
			   UsernameElement.appendChild(document.createTextNode(_loginHandler.get_uname()));
			      
			   Element PasswordElement = document.createElement("Password");
			   PasswordElement.appendChild(document.createTextNode(_loginHandler.get_password()));
			   
			   Element FirstNameElement = document.createElement("RealName");
			   FirstNameElement.appendChild(document.createTextNode(_loginHandler.get_RealName()));
			   
			   Element LastNameElement = document.createElement("PuzzleData");
			   LastNameElement.appendChild(document.createTextNode(ArraytoString(_puzzle)));
			   
			   Element PrevElement = document.createElement("PrevPuzzleData");
			   PrevElement.appendChild(document.createTextNode(Puzzles));
			   
			   Element ScoresElement = document.createElement("Scores");
			   ScoresElement.appendChild(document.createTextNode(Scores));
			   
			   rootElement.appendChild(State);
			   rootElement.appendChild(UsernameElement);
			   rootElement.appendChild(PasswordElement);
			   rootElement.appendChild(FirstNameElement);
			   rootElement.appendChild(LastNameElement);
			   rootElement.appendChild(ScoresElement);
			   rootElement.appendChild(PrevElement);
		

				// refer to the lecture slides to add the XML elements to
				// the document
				return document;
			} catch(Exception e){}
			return null;
		}
		
		private String generateXMLString() {
			Document document = generateClose();
			if (document != null) {
				try {
					TransformerFactory factory = TransformerFactory.newInstance();
					 Transformer transformer = factory.newTransformer();
					 Properties outFormat = new Properties();
					 outFormat.setProperty(OutputKeys.INDENT, "yes");
					 outFormat.setProperty(OutputKeys.METHOD, "xml");
					 outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
					 outFormat.setProperty(OutputKeys.VERSION, "1.0");
					 outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
					 transformer.setOutputProperties(outFormat);
					 DOMSource domSource = new DOMSource(document.getDocumentElement());
					 OutputStream output = new ByteArrayOutputStream();
					 StreamResult streamResult = new StreamResult(output);
					 transformer.transform(domSource, streamResult);
					 String xmlString = output.toString();
					return xmlString;
				} catch(Exception e) {}
			}
			return null; 
		}

		public void outputToFileStream(String Name) {
		    FileOutputStream outputStream;
		    String xmlString = generateXMLString();

		    if (xmlString != null) {
		    	try {
				    outputStream = openFileOutput(Name, Context.MODE_PRIVATE);
				    outputStream.write(xmlString.getBytes());
					outputStream.close();
				} catch (Exception e) {}
		    }
		}
		
		public void UserManagementClick(View v) {
			Intent nextScreen = new Intent(this,UserDetailsClass.class);
			float hi = PuzzleGrid.getX();
			PuzzleString = ArraytoString(_puzzle);
			nextScreen.putExtra("Puzzle", PuzzleString);
			nextScreen.putExtra("PreviousSolutions", Puzzles);
			nextScreen.putExtra("Scores", Scores);
			
			this.startActivity(nextScreen);
			this.finish();
		}

	 	public String ArraytoString(Puzzle puzzle){
	 			JSONObject newobj = new JSONObject();
	 			try {
					newobj.put("Width", puzzle.width);
					newobj.put("Id",puzzle.Id);
					newobj.put("Finish", puzzle.finished);
					newobj.put("Date", _puzzle.Date);
					newobj.put("TPL", TodaysPuzzleLoaded);
					JSONArray chars = new JSONArray();
					for(String s: puzzle.Characters){
						JSONObject obj = new JSONObject();
						obj.put("String", s);
						chars.put(obj);
					}
					newobj.put("Characters", chars);
					JSONArray words = new JSONArray();
					for(Word _word: puzzle.WordsArray){
						JSONObject word = new JSONObject();
						word.put("Column", _word.Column);
						word.put("Row", _word.Row);
						word.put("Length", _word.length);
						word.put("Direction", _word.Direction);
						word.put("Found", _word.Found);
						word.put("Text", _word.Text);
						words.put(word);
					}
					newobj.put("Words", words);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
	 		return  newobj.toString();
	 	}
	 	
	 	public Puzzle StringtoArray(String input){
			Puzzle _puzzle = new Puzzle();
	 		try {
				JSONObject jpuzzle = new JSONObject(input);

				_puzzle.width = Integer.parseInt(jpuzzle.optString("Width"));
				_puzzle.Id = jpuzzle.optString("Id");
				_puzzle.finished = jpuzzle.optBoolean("Finish");
				_puzzle.Date = jpuzzle.optString("Date");
				TodaysPuzzleLoaded = jpuzzle.optBoolean("TPL");
				JSONArray chars = jpuzzle.getJSONArray("Characters");
				String [] charsstring = new String[chars.length()];
				for(int i = 0; i < chars.length(); i++){
					JSONObject obj = chars.getJSONObject(i);
					charsstring[i] = obj.optString("String");
				}
				_puzzle.Characters = charsstring;
				JSONArray words = jpuzzle.getJSONArray("Words");
				Word [] WordArray = new Word [words.length()];
				for(int i = 0; i < words.length(); i++){
					JSONObject obj = words.getJSONObject(i);
					Word Singleword = new Word(obj.optString("Text"));
					Singleword.Row = obj.optInt("Row");
					Singleword.Column =  obj.optInt("Column");
					Singleword.Direction =  obj.optInt("Direction");
					Singleword.length =  obj.optInt("Length");
					Singleword.Found = obj.optBoolean("Found");
					WordArray[i] = Singleword;
				}
				_puzzle.WordsArray = WordArray;
			} catch (JSONException e) {
				e.printStackTrace();
			}
	 		return _puzzle;
	 	}
	 	
		public void LoadPuzzles(View v) {
			Intent nextScreen = new Intent(this,PreviousPuzzlesClass.class);
			PuzzleString = ArraytoString(_puzzle);
			nextScreen.putExtra("Puzzle", PuzzleString);
			nextScreen.putExtra("PreviousSolutions", Puzzles);
			nextScreen.putExtra("Scores", Scores);
			this.startActivity(nextScreen);
			this.finish();
		}
		
		@Override
		public void onTaskCompleted(String responseData) {
			JSONObject json;
			try {
				pdialog.dismiss();
				json = new JSONObject(responseData);
				_puzzle = new Puzzle();
				_puzzle.Id = json.optString("Id");
				_puzzle.Date = GetDate();
				_puzzle.finished = false;
				TodaysPuzzleLoaded = true;
				JSONArray puzzlearray = json.getJSONArray("Puzzle");
				String [] outputstrings = new String[puzzlearray.length()];
				for(int i =0; i < puzzlearray.length(); i++){
					outputstrings[i] = (String) puzzlearray.get(i);
				}
				
				_puzzle.width =outputstrings[0].length();
				_puzzle.Characters = SingleLetters(outputstrings);
				JSONArray jsonwordsarray = json.getJSONArray("Words");
				Word [] wordarray = new Word[jsonwordsarray.length()];
				for(int i =0; i < jsonwordsarray.length(); i++){
					String obj = (String) jsonwordsarray.get(i);
					Word temp = new Word(obj);
					temp.length = obj.length();
					wordarray[i] = temp;
				}
				
				_puzzle.WordsArray = wordarray;
				
				DrawGrids();     
			} catch (Exception e) {
				e.printStackTrace();
			}

			
		}
		
		public void DrawGrids(){
			for(int i = 0; i < _puzzle.WordsArray.length; i++){ //For Each Word
				Word temp = _puzzle.WordsArray[i]; //Get Current Word
				if(temp.length > longestWord)
					longestWord = temp.length;
				if(temp.length < shortestWord)
					shortestWord = temp.length;
			}
			
			PuzzleGrid = (GridView)findViewById(R.id.PuzzleGrid);
			PuzzleGrid.setNumColumns(_puzzle.width);
			PuzzleGrid.setAdapter(new TextViewAdapter(this,_puzzle.Characters,1,false));

			PuzzleGrid.setOnTouchListener(MyOnTouchListener);
			
			String [] tempWordsArray = new String[_puzzle.WordsArray.length];
			for(int i = 0; i < _puzzle.WordsArray.length; i++){ //For Each Word
				Word temp = _puzzle.WordsArray[i]; //Get Current Word
				tempWordsArray[i] = temp.Text;
			}
			wordsgrid = (GridView)findViewById(R.id.WordGrid);
			wordsgrid.setAdapter(new TextViewAdapter(this,tempWordsArray,longestWord,false));
			
			GenerateSolutions();
		}
		
		public String[] SingleLetters(String[] text){
		
		    String Letters = "";
		 
		    for(int i = 0; i < text.length; i++){ //Make a huge string
		    	Letters += text[i];
		    }
		    
		    char [] Letterchar = Letters.toCharArray();
		    String [] SingleLetterArray = new String[Letterchar.length];
		    for(int i = 0; i < Letterchar.length; i++){ //Convert char[] to string[]
		    	SingleLetterArray[i] = "" + Letterchar[i];
		    }
		    return SingleLetterArray;
		}

		int Direction = -1;
		
		boolean LineControl = false;

		public void CheckWord2(int pos, String character){
			try {
			TextView text2 = (TextView) PuzzleGrid.getAdapter().getItem(pos); //Get Text
			if(!_puzzle.finished){
							
			if(text2.getCurrentTextColor()!=0XFF00FF00){ //Already Been Selected

									CharArray += character;
									PosSelected[PosInArray++] = pos;
									int DirectiontoGo = -1;
									
									int length = -1;
									for(int i = 0; i < PosSelected.length; i++){
										if(PosSelected[i] == -1)
											break;
										length++;
									}
									
									
									if(PosInArray > 1)
										DirectiontoGo = CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns());
									
									if(PosInArray == 1)
										LineControl = true;
									
									if(PosInArray == 2){
										if(CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns()) == -1){  //make sure they are inline
											LineControl = false;
										} else {
											Direction = CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns());
											LineControl = true;
										}
									} else if (PosInArray == 3){
										if(DirectiontoGo != -1 && DirectiontoGo != Direction){
											TextView textpos2 = (TextView) PuzzleGrid.getAdapter().getItem(PosSelected[1]); //Get Text
											textpos2.setTextColor(0xFFFFFFFF);
											PosSelected[1] = PosSelected[2];
											PosSelected[2] = -1;
											PosInArray--;
											Direction = CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns());
											LineControl = true;
										} else if(DirectiontoGo == Direction) {
											LineControl = true;
										} else {
											LineControl = false;
										}
									
									} else if(PosInArray > 3){ //don't start checking until the shortest word
										if(DirectiontoGo != -1 && DirectiontoGo != Direction){
											LineControl = false; //If Direction is -1
										} else if(DirectiontoGo == Direction) { //if in a line
											LineControl = true;
										} else {
											LineControl = false; //If Direction is -1
										}
									}
								} else { //Already Been Selected
									LineControl = false;
								}
							} else { //Puzzle has been played
								LineControl = false;
							}
							
							if(LineControl == true){
								if(text2.getCurrentTextColor() == SelectedColour){ //Equal Blue
									text2.setTextColor(0xFFFF0000); //Red
								} else if (text2.getCurrentTextColor() == 0XFFFFFFFF) { //Equal White
									text2.setTextColor(0xFF00FF00);
								}
							}
			} catch(Exception e){}
		}
	
		
		public void IsItaWord(){
			Boolean aWordMatches = false;
		
			
			for(int i = 0; i < _puzzle.WordsArray.length; i++){ //For Each Word 
				Word temp = _puzzle.WordsArray[i]; //Get Current Word
				if(WordMatches(CharArray,temp) != WordStatus.NOMATCH){ //If We have some kind of match
					if (WordMatches(CharArray,temp) == WordStatus.PARTIALMATCH || WordMatches(CharArray,temp) == WordStatus.BACKWARDPARTIAL){
						aWordMatches = false;
					} else { //If Word is Matched
						final int size = _puzzle.WordsArray.length; //Amount of Words
						if(WordMatches(CharArray,temp) == WordStatus.BACKWARDMATCH){
							String reverse = "";
							int [] ReversePositions = new int[PosSelected.length];
							Arrays.fill(ReversePositions, -1);
							int position = 0;
							 for ( int h = CharArray.length() - 1 ; h >= 0 ; h-- ){
						         reverse = reverse + CharArray.charAt(h);
						         ReversePositions[position] = PosSelected[h];
						         position++;
							 }
							 CharArray = reverse;
							 PosSelected = ReversePositions;
						}

						for(int k = 0; k < size; k++) { //Loops through words
							 Adapter adapter = wordsgrid.getAdapter();
							 TextView text = (TextView) adapter.getItem(k); //Get Text
							 String textviewtext = text.getText().toString();
							if(textviewtext.equals(CharArray)){ //If Word searching is the one we are looking for
								 text.setTextColor(0XFFFF0000); //Set the text colour to red
								 text.setPaintFlags(text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
							}
						}
						
						UpdateWordStatus(temp, PosSelected);
						
						for(int j = 0; j < PosInArray; j++){
							TextView textview = (TextView) PuzzleGrid.getAdapter().getItem(PosSelected[j]); //Get Text
							textview.setTextColor(SelectedColour);
							PosSelected[j] = 0;
						}
						CharArray = "";
						PosInArray = 0;
					}
				}
			}
			
			if(!aWordMatches){ //If the word doesn't exist
				ResetGrid();
			}
		}
		
		
		public void CheckWord(View v2, int pos, String character){
			
			if(!_puzzle.finished){
			
			Adapter adapter2 = PuzzleGrid.getAdapter();
			 TextView text2 = (TextView) adapter2.getItem(pos); //Get Text

			 if(text2.getCurrentTextColor()!=0XFF00FF00){
			Views[PosInArray] = text2;
			if(text2.getCurrentTextColor() == SelectedColour){ //Equal Blue
				text2.setTextColor(0xFFFF0000); //Red
			} else if (text2.getCurrentTextColor() == 0XFFFFFFFF) { //Equal White
				text2.setTextColor(0xFF00FF00);
			}
			CharArray += character;
			PosSelected[PosInArray++] = pos;
			Boolean aWordMatches = false;
			
			if(PosInArray == 2){
				if(CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns()) == -1){  //make sure they are inline
					ResetGrid();
				} else {
					Direction = CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns());
				}
			} else if(shortestWord > PosInArray && PosInArray > 2){
				if(CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns()) != Direction){
					ResetGrid();
				}
			} else if(shortestWord <= PosInArray){ //don't start checking until the shortest word
				if(CalculateDirection(PosSelected[PosInArray - 1], PosSelected[PosInArray - 2], PuzzleGrid.getNumColumns()) == Direction) {
					for(int i = 0; i < _puzzle.WordsArray.length; i++){ //For Each Word 
						Word temp = _puzzle.WordsArray[i]; //Get Current Word
						if(WordMatches(CharArray,temp) != WordStatus.NOMATCH){ //If We have some kind of match
							if (WordMatches(CharArray,temp) == WordStatus.PARTIALMATCH || WordMatches(CharArray,temp) == WordStatus.BACKWARDPARTIAL){
								aWordMatches = true;
							} else { //If Word is Matched
								final int size = _puzzle.WordsArray.length; //Amount of Words
								if(WordMatches(CharArray,temp) == WordStatus.BACKWARDMATCH){
									String reverse = "";
									int [] ReversePositions = new int[PosSelected.length];
									Arrays.fill(ReversePositions, -1);
									int position = 0;
									 for ( int h = CharArray.length() - 1 ; h >= 0 ; h-- ){
								         reverse = reverse + CharArray.charAt(h);
								         ReversePositions[position] = PosSelected[h];
								         position++;
									 }
									 CharArray = reverse;
									 PosSelected = ReversePositions;
								}
		
								for(int k = 0; k < size; k++) { //Loops through words
									 Adapter adapter = wordsgrid.getAdapter();
									 TextView text = (TextView) adapter.getItem(k); //Get Text
									 String textviewtext = text.getText().toString();
									if(textviewtext.equals(CharArray)){ //If Word searching is the one we are looking for
										 text.setTextColor(0XFFFF0000); //Set the text colour to red
										 text.setPaintFlags(text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
									}
								}
								
								UpdateWordStatus(temp, PosSelected);
								
								for(int j = 0; j < PosInArray; j++){
									PosSelected[j] = 0;
									Views[j].setTextColor(SelectedColour);
									Views[j] = null;
								}
								CharArray = "";
								PosInArray = 0;
							}
						}
					}
					
					if(!aWordMatches){ //If the word doesn't exist
						ResetGrid();
					}
				} else {
					ResetGrid();
				}
			}
			 }
			}
		}
		
		public void ResetGrid(){
			
			for(int i = 0; i < PosInArray; i++){
				Adapter adapter2 = PuzzleGrid.getAdapter();
				 TextView text2 = (TextView) adapter2.getItem(PosSelected[i]); //Get Text
				 
				if(text2.getCurrentTextColor() == 0XFF00FF00) { //Green
					text2.setTextColor(0XFFFFFFFF);
				} else if(text2.getCurrentTextColor() == 0XFFFF0000) { //If Red
					text2.setTextColor(SelectedColour);
				}
				PosSelected[i] = -1;
			}
			CharArray = "";
			PosInArray = 0;
		}
		
		private void UpdateWordStatus(Word _temp, int[] _posSelected) {
			
	
			int[] _newarray = new int[_temp.length]; //Create an Array length of the word
			for(int i =0; i < _temp.length; i ++){ //for each character
				_newarray[i] = _posSelected[i]; //assign the position 
			}
			
			int dir = GetDirection(_newarray);
			
			int InitialRow = (int)(_newarray[0])/ PuzzleGrid.getNumColumns(); //starts at 0
			int Row = (PuzzleGrid.getNumColumns() - 1) - InitialRow; //the -1 is for starting at zero
			int Column = _newarray[0]  - (PuzzleGrid.getNumColumns() * InitialRow);
			_temp.Row = Row;
			_temp.Column = Column;
			_temp.Direction = dir;
			_temp.Found = true;
			
			UploadPuzzle();
		}
		
		public int GetDirection(int[] _posSelected){
			int Direction = -1;
			for(int i = 0; i < (_posSelected.length - 1); i++){
				if(Direction == -1){
					Direction = CalculateDirection(_posSelected[i], _posSelected[i + 1], PuzzleGrid.getNumColumns());
				} else {
					if(Direction != CalculateDirection(_posSelected[i], _posSelected[i + 1], PuzzleGrid.getNumColumns())){
						return -1;
					}
				}
			}
			return Direction;
		}
		
		public int CalculateDirection(int pos1, int pos2, int gridlength){
			if(pos1 == (pos2 - 1)) { //if it is one less i.e. 3 EAST
				return 4;
			} else if (pos1 == (pos2 + 1)) { //if it is one less i.e. 4 WEST
				return 3;
			} else if ((pos1 + gridlength) == pos2) {//if it is the length of the grid plus pos1 SOUTH
				return 1;
			} else if ((pos1 - gridlength) == pos2) {//if it is the length of the grid minus pos1 NORTH
				return 6;
			} else if ((pos1 - gridlength - 1) == pos2) {//if it is the length of the grid minus pos1 - 1 NORTH WEST
				return 5;
			} else if ((pos1 - gridlength + 1) == pos2) {//if it is the length of the grid minus pos1 + 1 NORTH EAST
				return 7;
			} else if ((pos1 + gridlength - 1) == pos2) {//if it is the length of the grid plus pos1 - 1 SOUTH WEST
				return 0;
			} else if ((pos1 + gridlength + 1) == pos2) {//if it is the length of the grid plus pos1 + 1 SOUTH EAST
				return 2;
			} else {
				return -1;
			}
		}

		public WordStatus WordMatches(String compare, Word _word){
			for(int i = 0;i < compare.length(); i++) {
				if(_word.Text.charAt(i) != compare.charAt(i)) {
					if(_word.Text.charAt(_word.length - i - 1)!= compare.charAt(i))
						return WordStatus.NOMATCH;
				}
			}
			if(compare.equals(_word.Text.substring(0, compare.length())))
			{
				if(_word.length == compare.length()){
					return WordStatus.MATCH;
				} else {
					return WordStatus.PARTIALMATCH;
				}
			} else {
				if(_word.length == compare.length()){
					return WordStatus.BACKWARDMATCH;
				} else {
					return WordStatus.BACKWARDPARTIAL;
				}
			}
		}
	
		public void UploadPuzzle(){
			try {
				JSONObject jsonpuzzle = new JSONObject();
				JSONArray solution = new JSONArray();
	
				Boolean ReadyForUpload = true;
				
				for(int i = 0; i < _puzzle.WordsArray.length; i++){
					if(_puzzle.WordsArray[i].Found == false)
						ReadyForUpload = false;
				}
			
				if(ReadyForUpload){
					for(int i = 0; i < _puzzle.WordsArray.length; i++){
						JSONObject word = new JSONObject();
						word.put("Direction", _puzzle.WordsArray[i].Direction);
						word.put("Column", _puzzle.WordsArray[i].Column);
						word.put("Row", _puzzle.WordsArray[i].Row);
						word.put("Word", _puzzle.WordsArray[i].Text);
						solution.put(word);
					}
					jsonpuzzle.put("SolutionWords", solution);
					jsonpuzzle.put("Password", _loginHandler.get_password());
					jsonpuzzle.put("Username", _loginHandler.get_uname());
					jsonpuzzle.put("Id", _puzzle.Id);
					
			        String url = "http://08309.net.dcs.hull.ac.uk/api/wordsearch/submit";
					SendResponseData sendResponseData = new SendResponseData(this);
					sendResponseData.execute(url,jsonpuzzle.toString());
					
				}
			
			} catch(Exception e){}
		}
		
		@Override
		public void onSendCompleted(String responseData) {

			if(responseData.equals("INVALID")){
				Toast.makeText(getApplicationContext(), "Invalid", Toast.LENGTH_SHORT).show();
			} else if(responseData.equals("WRONG")){
				Toast.makeText(getApplicationContext(), "Something is seriously broken right now", Toast.LENGTH_SHORT).show();
			} else if(responseData.equals("CORRECT")){
				Toast.makeText(getApplicationContext(), "WINNER!!!!", Toast.LENGTH_SHORT).show();
				_puzzle.finished = true;
			} else {
				Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
			}
			
		}

		public void GenerateSolutions(){
			int FoundCount = 0;
			for(int i= 0; i < _puzzle.WordsArray.length; i++){
				Word obj = _puzzle.WordsArray[i];
				rowcount = (PuzzleGrid.getCount() / _puzzle.width); //because it starts at 0
				if(obj.Found) {
					int [] _pos = Positions(obj.Direction,obj.Row,obj.Column,obj.Text.length());
					FoundCount++;
					for(int j =0; j < _pos.length; j++){
						 Adapter hello = PuzzleGrid.getAdapter();
						 TextView text = (TextView) hello.getItem(_pos[j]);
						text.setTextColor(SelectedColour);
					}
					final int size = _puzzle.WordsArray.length; //Amount of Words
					for(int k = 0; k < size; k++) { //Loops through words
						 Adapter adapter = wordsgrid.getAdapter();
						 TextView text = (TextView) adapter.getItem(k); //Get Text
						 String textviewtext = text.getText().toString();
						if(textviewtext.equals(obj.Text)){ //If Word searching is the one we are looking for
							 text.setTextColor(0XFFFF0000); //Set the text colour to red
							 text.setPaintFlags(text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
						}
					}
				
				}
			}
			if(_puzzle.WordsArray.length == FoundCount){
				UploadPuzzle();
			}
			
		}
		int rowcount = 0;
		
		public int[] Positions(int Direction, int row, int column, int length){
			int [] _Positions = new int[length];

			int correctionrow = (rowcount - (row + 1));
			
			_Positions[0] = (correctionrow * _puzzle.width) + column;
			
			for(int i = 1; i < length; i++){
				_Positions[i] = ReturnPosFromDirection(_Positions[i-1], Direction);
			}
			
			return _Positions;
		}

		public int ReturnPosFromDirection(int pos1, int Direction){
			switch(Direction){
				case 0:
					return (pos1 + _puzzle.width - 1);
				case 1:
					return (pos1 + _puzzle.width);
				case 2:
					return (pos1 + _puzzle.width + 1);
				case 3:
					return (pos1 - 1);
				case 4:
					return (pos1 + 1);
				case 5:
					return (pos1 - _puzzle.width - 1);
				case 6:
					return (pos1 - _puzzle.width);
				case 7:
					return (pos1 - _puzzle.width + 1);
			}
			return -1;
		}


		
		
}


