package com.example.angrywordsearch;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class ChangePasswordClass extends Activity implements OnRetrieveDataCompleted {
	LoginHandler _loginHandler;
	String ConPass, NewPass,PuzzleString = "", PrevPuzzle = "", PrevScoresString = "";
	ProgressDialog pdialog;
	 TextView passwordtext;
	 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    setContentView(R.layout.changepassword);
	    _loginHandler = LoginParent._loginHandler;
	    passwordtext = (TextView) findViewById(R.id.currentPasswordText);
	    TextView npasswordtext = (TextView) findViewById(R.id.NewPasswordText);
	    TextView cpasswordtext = (TextView) findViewById(R.id.ConfirmPasswordText);
	    npasswordtext.setNextFocusDownId(R.id.NewPasswordText);
	    cpasswordtext.setNextFocusDownId(R.id.ConfirmPasswordText);
	    Intent i= getIntent();
    	try {
    		String NewPass = "", ConfirmPass = "";
	    	  if(i.getExtras() != null){
			    	try{
			    		PuzzleString = i.getStringExtra("Puzzle");
			    	} catch(Exception e){}
			    	try{
			    		PrevPuzzle = i.getStringExtra("PreviousSolutions");
			    	} catch(Exception e){}
			    	try{
			    		PrevScoresString = i.getStringExtra("Scores");
			    	} catch(Exception e){}
			    	try{
			    		NewPass = i.getStringExtra("FirstPassword");
						ConfirmPass = i.getStringExtra("SecondPassword");
			    	} catch(Exception e){}
			    } 
	    	npasswordtext.setText(NewPass);
	    	cpasswordtext.setText(ConfirmPass);
    	} catch (Exception e){}
	    passwordtext.setText(_loginHandler.get_password());
	    
	    Switch toggle = (Switch) findViewById(R.id.switch1);
	    toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
	        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	            if (isChecked) {
	            	passwordtext.setInputType(InputType.TYPE_CLASS_TEXT);
	            } else {
	            	passwordtext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
	            }
	        }
	    });
	 }

    protected void onStop() {
	    super.onStop();
	    //outputToFileStream("end.xml");
    }
    
    @Override
    protected void onPause() {
	    super.onPause();
	   // outputToFileStream("end.xml");
    }

	private Document generateClose() {
		 TextView passwordtext = (TextView) findViewById(R.id.NewPasswordText);
		 TextView cpasswordtext = (TextView) findViewById(R.id.ConfirmPasswordText);
		 ConPass = cpasswordtext.getText().toString();
		 NewPass = passwordtext.getText().toString();
		    
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();
			
			Element rootElement = document.createElement("State");
			   document.appendChild(rootElement);

			Element State = document.createElement("PrevState");
			State.appendChild(document.createTextNode("ChangePassword"));
			
		   Element UsernameElement = document.createElement("Username");
		   UsernameElement.appendChild(document.createTextNode(_loginHandler.get_uname()));
		      
		   Element PasswordElement = document.createElement("Password");
		   PasswordElement.appendChild(document.createTextNode(_loginHandler.get_password()));
		   
		   Element FirstNameElement = document.createElement("RealName");
		   FirstNameElement.appendChild(document.createTextNode(_loginHandler.get_RealName()));
		   
		   Element PuzzleElement = document.createElement("PuzzleData");
		   PuzzleElement.appendChild(document.createTextNode(PuzzleString));
		   
		   Element FirPasswordElement = document.createElement("FirstPassword");
		   FirPasswordElement.appendChild(document.createTextNode(NewPass));
		   
		   Element SecPasswordElement = document.createElement("SecondPassword");
		   SecPasswordElement.appendChild(document.createTextNode(ConPass));
		   
		   Element PrevPuzzleData = document.createElement("PrevPuzzle");
		   PrevPuzzleData.appendChild(document.createTextNode(PrevPuzzle));
		   
		   Element PrevScores = document.createElement("PrevScores");
		   PrevScores.appendChild(document.createTextNode(PrevScoresString));
		   
		   rootElement.appendChild(State);
		   rootElement.appendChild(UsernameElement);
		   rootElement.appendChild(PasswordElement);
		   rootElement.appendChild(FirstNameElement);
		   rootElement.appendChild(PuzzleElement);
		   rootElement.appendChild(FirPasswordElement);
		   rootElement.appendChild(SecPasswordElement);
		   rootElement.appendChild(PrevPuzzleData);
		   rootElement.appendChild(PrevScores);
		   
	

			// refer to the lecture slides to add the XML elements to
			// the document
			return document;
		} catch(Exception e){}
		return null;
	}
	
	private String generateXMLString() {
		Document document = generateClose();
		if (document != null) {
			try {
				TransformerFactory factory = TransformerFactory.newInstance();
				 Transformer transformer = factory.newTransformer();
				 Properties outFormat = new Properties();
				 outFormat.setProperty(OutputKeys.INDENT, "yes");
				 outFormat.setProperty(OutputKeys.METHOD, "xml");
				 outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
				 outFormat.setProperty(OutputKeys.VERSION, "1.0");
				 outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
				 transformer.setOutputProperties(outFormat);
				 DOMSource domSource = new DOMSource(document.getDocumentElement());
				 OutputStream output = new ByteArrayOutputStream();
				 StreamResult streamResult = new StreamResult(output);
				 transformer.transform(domSource, streamResult);
				 String xmlString = output.toString();
				return xmlString;
			} catch(Exception e) {}
		}
		return null; 
	}

	public void outputToFileStream(String Name) {
	    FileOutputStream outputStream;
	    String xmlString = generateXMLString();

	    if (xmlString != null) {
	    	try {
			    outputStream = openFileOutput(Name, Context.MODE_PRIVATE);
			    outputStream.write(xmlString.getBytes());
				outputStream.close();
			} catch (Exception e) {}
	    }
	}

	public void BackClick(View v) {
		Intent nextScreen = new Intent(this,UserDetailsClass.class);
		nextScreen.putExtra("Puzzle", PuzzleString);
		nextScreen.putExtra("PreviousSolutions", PrevPuzzle);
		nextScreen.putExtra("Score", PrevScoresString);
		this.startActivity(nextScreen);
		this.finish();
	}
	
	public void ConfirmClick(View v) {
	    TextView passwordtext = (TextView) findViewById(R.id.NewPasswordText);
	    TextView cpasswordtext = (TextView) findViewById(R.id.ConfirmPasswordText);
	    ConPass = cpasswordtext.getText().toString();
	    NewPass = passwordtext.getText().toString();
	    
	    if(ConPass.equals(NewPass) && !ConPass.equals(""))
	    {
	    	String url = "http://08309.net.dcs.hull.ac.uk/api/admin/change?username=" + _loginHandler.get_uname() + "&oldpassword=" + _loginHandler.get_password() + "&newpassword=" + NewPass;
			RetrieveResponseData retrieveResponseData = new RetrieveResponseData(this);
			retrieveResponseData.execute(url);
			pdialog=new ProgressDialog(this);
			pdialog.setCancelable(true);
			pdialog.setMessage("Loading ....");
			pdialog.show();
	    } else {
	    	Toast.makeText(getApplicationContext(), "Passwords must be the same and not empty!", Toast.LENGTH_SHORT).show();
	    }
	}

	@Override
	public void onTaskCompleted(String responseData) {
		JSONObject json;
		try {
			pdialog.dismiss();
			json = new JSONObject(responseData);
			String containserrors = json.optString("Error");
			if(containserrors.equals(""))
			{
				Toast.makeText(getBaseContext(),"Password Changed!", Toast.LENGTH_SHORT).show();
				_loginHandler.set_password(NewPass);
				Intent nextScreen = new Intent(this,UserDetailsClass.class);
				nextScreen.putExtra("Puzzle", PuzzleString);
				nextScreen.putExtra("PreviousSolutions", PrevPuzzle);
				nextScreen.putExtra("Score", PrevScoresString);
				this.startActivity(nextScreen);
				this.finish();
			} else {
				 Toast.makeText(getApplicationContext(), "Couldn't Change Password", Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
