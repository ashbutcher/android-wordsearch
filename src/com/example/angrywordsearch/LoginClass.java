package com.example.angrywordsearch;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginClass extends Activity implements OnRetrieveDataCompleted {

	
	LoginHandler _loginHandler;
	String Password, Username = "";
	EditText  unamebox, upassbox;
	ProgressDialog pdialog;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
		    setContentView(R.layout.userlogin);
		    _loginHandler = LoginParent._loginHandler;
		    
		    	Intent i= getIntent();
			  unamebox = (EditText) findViewById(R.id.LoginUsername);
			  upassbox = (EditText) findViewById(R.id.LoginPasswordText);
			  
			  unamebox.setNextFocusDownId(R.id.LoginUsername);
			  upassbox.setNextFocusDownId(R.id.LoginPasswordText);
			  
			  try {
			  Username = i.getStringExtra("Username");
			  Password = i.getStringExtra("Password");
			  unamebox.setText(Username);
			  upassbox.setText(Password);
			  } catch(Exception e){}

		 }

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.angry_wordsearch, menu);
			return true;
		}
		
	    @Override
	    protected void onStop() {
		    super.onStop();
		    outputToFileStream("end.xml");
	    }
	    
	    @Override
	    protected void onPause() {
		    super.onPause();
		    outputToFileStream("end.xml");
	    }
		
		public void LoginClick(View v)
		{
			  EditText unamebox = (EditText) findViewById(R.id.LoginUsername);
			  EditText upassbox = (EditText) findViewById(R.id.LoginPasswordText);
			  
			  Username = unamebox.getText().toString();
			  Password = upassbox.getText().toString();
			  
			  if(!Username.equals("") && !Password.equals("")) {
					String url = "http://08309.net.dcs.hull.ac.uk/api/admin/details?username=" + unamebox.getText().toString().trim() + "&password=" + upassbox.getText().toString().trim();
					RetrieveResponseData retrieveResponseData = new RetrieveResponseData(this);
					retrieveResponseData.execute(url);
					pdialog=new ProgressDialog(this);
					pdialog.setCancelable(true);
					pdialog.setMessage("Loading ....");
					pdialog.show();
					} else {
						 Toast.makeText(getApplicationContext(), "Username and/or Password are blank", Toast.LENGTH_SHORT).show();
					}
			  

		}
		
		@Override
		public void onTaskCompleted(String responseData) {
			
				JSONObject json;
				try {
					pdialog.dismiss();
					json = new JSONObject(responseData);
					String containserrors = json.optString("Error");
					if(containserrors.equals(""))
					{
						Toast.makeText(getBaseContext(),"Login Successful", Toast.LENGTH_SHORT).show();
						String Name = json.optString("FullName");
						_loginHandler.LoginUser(Username, Name, Password);
						Intent nextScreen = new Intent(this,MainPuzzleClass.class);
						this.startActivity(nextScreen);
						this.finish();
					} else {
						 Toast.makeText(getApplicationContext(), containserrors, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		
		private Document generateClose() {
			
			  EditText unamebox = (EditText) findViewById(R.id.LoginUsername);
			  EditText upassbox = (EditText) findViewById(R.id.LoginPasswordText);
			  
			  Username = unamebox.getText().toString();
			  Password = upassbox.getText().toString();
			
			try {
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document document = documentBuilder.newDocument();
				
				Element rootElement = document.createElement("State");
				   document.appendChild(rootElement);

				Element State = document.createElement("PrevState");
				State.appendChild(document.createTextNode("Login"));
				
			   Element UsernameElement = document.createElement("Username");
			   UsernameElement.appendChild(document.createTextNode(Username));
			      
			   Element PasswordElement = document.createElement("Password");
			   PasswordElement.appendChild(document.createTextNode(Password));   
			   
			   rootElement.appendChild(State);
			   rootElement.appendChild(UsernameElement);
			   rootElement.appendChild(PasswordElement);

				// refer to the lecture slides to add the XML elements to
				// the document
				return document;
			} catch(Exception e){}
			return null;
		}
		
		private String generateXMLString() {
			Document document = generateClose();
			if (document != null) {
				try {
					TransformerFactory factory = TransformerFactory.newInstance();
					 Transformer transformer = factory.newTransformer();
					 Properties outFormat = new Properties();
					 outFormat.setProperty(OutputKeys.INDENT, "yes");
					 outFormat.setProperty(OutputKeys.METHOD, "xml");
					 outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
					 outFormat.setProperty(OutputKeys.VERSION, "1.0");
					 outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
					 transformer.setOutputProperties(outFormat);
					 DOMSource domSource = new DOMSource(document.getDocumentElement());
					 OutputStream output = new ByteArrayOutputStream();
					 StreamResult streamResult = new StreamResult(output);
					 transformer.transform(domSource, streamResult);
					 String xmlString = output.toString();
					return xmlString;
				} catch(Exception e) {}
			}
			return null; 
		}

		public void outputToFileStream(String Name) {
		    FileOutputStream outputStream;
		    String xmlString = generateXMLString();

		    if (xmlString != null) {
		    	try {
				    outputStream = openFileOutput(Name, Context.MODE_PRIVATE);
				    outputStream.write(xmlString.getBytes());
					outputStream.close();
				} catch (Exception e) {}
		    }
		}
		
}
