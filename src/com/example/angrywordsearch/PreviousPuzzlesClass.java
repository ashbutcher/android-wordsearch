package com.example.angrywordsearch;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


public class PreviousPuzzlesClass extends Activity  implements OnRetrieveDataCompleted {

	LoginHandler _loginHandler;
	String Id = "";
	GridView PuzzleGrid;
	String CharArray = new String();
	int PosInArray = 0, DateModifier = -1;
	String PuzzleString = "", Scores = "";
	Puzzle _puzzle;
	int Pos = 0;
	List<Puzzle> Puzzles;
	ProgressDialog pdialog;
	
	enum WordStatus {NOMATCH, PARTIALMATCH, MATCH, BACKWARDMATCH, BACKWARDPARTIAL};
	
	float dist0, distCurrent, currentXPosition, currentYPosition, currentX1Position, currentPosition;
	private static float MIN_ZOOM = -2f;
	private static float MAX_ZOOM = 5f;
	private float scaleFactor = 1.0f;
	private ScaleGestureDetector detector;
	
	float eventX = 0;
	float eventY = 0;
	float eventX1 = 0;
	float eventY1 = 0;
	
	//Touch event related variables
	int touchState;
	final int IDLE = 0;
	final int TOUCH = 1;
	final int PINCH = 2;
	final int DRAG = 3;
	TextView datetext;

	
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		
		@Override
			public boolean onScale(ScaleGestureDetector detector) {
			scaleFactor *= detector.getScaleFactor();
			scaleFactor = Math.max(MIN_ZOOM, Math.min(scaleFactor, MAX_ZOOM));
			PuzzleGrid.setScaleX(scaleFactor);
			PuzzleGrid.setScaleY(scaleFactor);
			return true;
			}
		}
	
	OnTouchListener MyOnTouchListener = new OnTouchListener(){
	
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			
			float distx, disty;
			currentXPosition = event.getX();
            currentYPosition = event.getY();

            switch(event.getAction() & MotionEvent.ACTION_MASK){
			case MotionEvent.ACTION_DOWN:
				//A pressed gesture has started, the motion contains the initial starting location.
				touchState = DRAG;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				touchState = PINCH;
				//Get the distance when the second pointer is touching
				eventX1 = event.getX(1);
				eventY1 = event.getY(1);
				
				distx = currentXPosition - eventX1;
				disty = currentYPosition- eventY1;
				
				dist0 = FloatMath.sqrt(distx * distx + disty * disty);

				break;
			case MotionEvent.ACTION_MOVE:
				if(touchState == PINCH){						
					eventX1 = event.getX(1);
					eventY1 = event.getY(1);

					distx = currentXPosition - eventX1;
					disty = currentYPosition- eventY1;
					
					distCurrent = FloatMath.sqrt(distx * distx + disty * disty);	
				}
				
				break;
			case MotionEvent.ACTION_UP:
				touchState = IDLE;
				break;
			case MotionEvent.ACTION_POINTER_UP: 
				touchState = DRAG;
				break;
			}
            detector.onTouchEvent(event);	
			return true;
			
		}
    };

	
		public String GetDate()  {
		Calendar cal=Calendar.getInstance();		
		cal.add(Calendar.DAY_OF_MONTH, DateModifier);
		Date currentdate = new Date(cal.getTimeInMillis());
		return currentdate.toString();
	}
	
		public String GetTodayDate()  {
		Calendar cal=Calendar.getInstance();		
		cal.add(Calendar.DAY_OF_MONTH, 0);
		Date currentdate = new Date(cal.getTimeInMillis());
		return currentdate.toString();
	}
	
		
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
		    setContentView(R.layout.previouspuzzles);
		    _loginHandler = LoginParent._loginHandler;
		    detector = new ScaleGestureDetector(this, new ScaleListener());
		    Puzzles = new ArrayList<Puzzle>();
		    Intent i = getIntent();
		    if(i.getExtras() != null)
		    {
		    	try{
		    		PuzzleString = i.getStringExtra("Puzzle");
		    	} catch(Exception e){}
		    	try{
		    		Puzzles = StringtoArray(i.getStringExtra("PreviousSolutions"));
		    	} catch(Exception e){}
		    	try{
		    		Scores = i.getStringExtra("Scores");
		    	} catch(Exception e){}
		    	
		    } 
		    	LoadPuzzles();
		 }
	 
	 	public void LoadPuzzles(){
	 		datetext = (TextView)findViewById(R.id.DateTime);
	 		Puzzle _internalpuzzle = null;
	 		String date = GetDate();
	 		boolean found = false;
	 		for(Puzzle _temppuzzle : Puzzles){
	 			if(_temppuzzle.Date.equals(date)){
	 				found = true;
	 				_internalpuzzle = _temppuzzle;
	 			}
	 		}
	 		
	 		if(found){ //if we are loading a saved puzzle
	 			_puzzle = _internalpuzzle;
	 			datetext.setText("Date: " + _puzzle.Date);
	 			DrawPuzzle();
	 		} else {
		        String url = "http://08309.net.dcs.hull.ac.uk/api/wordsearch/puzzle?date="+ GetDate();
				RetrieveResponseData retrieveResponseData = new RetrieveResponseData(this);
				retrieveResponseData.execute(url);
				pdialog=new ProgressDialog(this);
				pdialog.setCancelable(true);
				pdialog.setMessage("Loading ....");
				pdialog.show();
	 		}
	 	}
	 
	 	public String ArraytoString(List<Puzzle> _puzzles){
	 		String jsonstring = ""; 
	 		for(Puzzle puzzle : _puzzles){
	 			JSONObject newobj = new JSONObject();
	 			try {
					newobj.put("Width", puzzle.width);
					newobj.put("Date", puzzle.Date);
					newobj.put("Id", puzzle.Id);
					JSONArray chars = new JSONArray();
					for(String s: puzzle.Characters){
						JSONObject obj = new JSONObject();
						obj.put("String", s);
						chars.put(obj);
					}
					newobj.put("Characters", chars);
					JSONArray words = new JSONArray();
					for(Word _word: puzzle.WordsArray){
						JSONObject word = new JSONObject();
						word.put("Column", _word.Column);
						word.put("Row", _word.Row);
						word.put("Length", _word.length);
						word.put("Direction", _word.Direction);
						word.put("Found", _word.Found);
						word.put("Text", _word.Text);
						words.put(word);
					}
					newobj.put("Words", words);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	 			jsonstring += newobj.toString() + "#";
	 		}
	 		
	 		return jsonstring;
	 	}
	 	
	 	public List<Puzzle> StringtoArray(String input){
	 		String [] jsonstrings = input.split("#");
	 		List<Puzzle> puzzles = new ArrayList<Puzzle>();
	 		try {
	 			for(int puzzleid = 0; puzzleid < jsonstrings.length; puzzleid++) {
				JSONObject jpuzzle = new JSONObject(jsonstrings[puzzleid]);
				Puzzle _puzzle = new Puzzle();
				_puzzle.width = Integer.parseInt(jpuzzle.optString("Width"));
				_puzzle.Date = jpuzzle.optString("Date");
				_puzzle.Id = jpuzzle.optString("Id");
				JSONArray chars = jpuzzle.getJSONArray("Characters");
				String [] charsstring = new String[chars.length()];
				for(int i = 0; i < chars.length(); i++){
					JSONObject obj = chars.getJSONObject(i);
					charsstring[i] = obj.optString("String");
				}
				_puzzle.Characters = charsstring;
				JSONArray words = jpuzzle.getJSONArray("Words");
				Word [] WordArray = new Word [words.length()];
				for(int i = 0; i < words.length(); i++){
					JSONObject obj = words.getJSONObject(i);
					Word Singleword = new Word(obj.optString("Text"));
					Singleword.Row = obj.optInt("Row");
					Singleword.Column =  obj.optInt("Column");
					Singleword.Direction =  obj.optInt("Direction");
					Singleword.length =  obj.optInt("Length");
					if(obj.optString("Found").equals("True")){
						Singleword.Found = true;
					} else {
						Singleword.Found = false;
					}
					WordArray[i] = Singleword;
				}
				_puzzle.WordsArray = WordArray;
				puzzles.add(_puzzle);
	 			}
			} catch (JSONException e) {
				e.printStackTrace();
			}
	 		return puzzles;
	 	}
	 	
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.angry_wordsearch, menu);
			return true;
		}
		
	    protected void onStop() {
		    super.onStop();
		    outputToFileStream("end.xml");
	    }
	    
	    @Override
	    protected void onPause() {
		    super.onPause();
		    outputToFileStream("end.xml");
	    }

		private Document generateClose() {
			try {
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document document = documentBuilder.newDocument();
				
				Element rootElement = document.createElement("State");
				   document.appendChild(rootElement);

				Element State = document.createElement("PrevState");
				State.appendChild(document.createTextNode("PrevPuzzles"));
				
			   Element UsernameElement = document.createElement("Username");
			   UsernameElement.appendChild(document.createTextNode(_loginHandler.get_uname()));
			      
			   Element PasswordElement = document.createElement("Password");
			   PasswordElement.appendChild(document.createTextNode(_loginHandler.get_password()));
			   
			   Element FirstNameElement = document.createElement("RealName");
			   FirstNameElement.appendChild(document.createTextNode(_loginHandler.get_RealName()));
			   
			   Element LastNameElement = document.createElement("PuzzleData");
			   LastNameElement.appendChild(document.createTextNode(PuzzleString));
			   
			   Element PrevElement = document.createElement("PrevPuzzleData");
			   PrevElement.appendChild(document.createTextNode(ArraytoString(Puzzles)));
			   
			   Element ScoresElement = document.createElement("Scores");
			   ScoresElement.appendChild(document.createTextNode(Scores));
			   
			   rootElement.appendChild(State);
			   rootElement.appendChild(UsernameElement);
			   rootElement.appendChild(PasswordElement);
			   rootElement.appendChild(FirstNameElement);
			   rootElement.appendChild(LastNameElement);
			   rootElement.appendChild(PrevElement);
			   rootElement.appendChild(ScoresElement);
		

				// refer to the lecture slides to add the XML elements to
				// the document
				return document;
			} catch(Exception e){}
			return null;
		}
		
		private String generateXMLString() {
			Document document = generateClose();
			if (document != null) {
				try {
					TransformerFactory factory = TransformerFactory.newInstance();
					 Transformer transformer = factory.newTransformer();
					 Properties outFormat = new Properties();
					 outFormat.setProperty(OutputKeys.INDENT, "yes");
					 outFormat.setProperty(OutputKeys.METHOD, "xml");
					 outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
					 outFormat.setProperty(OutputKeys.VERSION, "1.0");
					 outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
					 transformer.setOutputProperties(outFormat);
					 DOMSource domSource = new DOMSource(document.getDocumentElement());
					 OutputStream output = new ByteArrayOutputStream();
					 StreamResult streamResult = new StreamResult(output);
					 transformer.transform(domSource, streamResult);
					 String xmlString = output.toString();
					return xmlString;
				} catch(Exception e) {}
			}
			return null; 
		}

		public void outputToFileStream(String Name) {
		    FileOutputStream outputStream;
		    String xmlString = generateXMLString();

		    if (xmlString != null) {
		    	try {
				    outputStream = openFileOutput(Name, Context.MODE_PRIVATE);
				    outputStream.write(xmlString.getBytes());
					outputStream.close();
				} catch (Exception e) {}
		    }
		}

		@Override
		public void onTaskCompleted(String responseData) {
			JSONObject json;
			Word[] WordArray;
			try {
				pdialog.dismiss();
				json = new JSONObject(responseData);
				JSONArray puzzle = json.getJSONArray("Puzzle");
				String [] outputstrings = new String[puzzle.length()];
				for(int i =0; i < puzzle.length(); i++){
					outputstrings[i] = puzzle.getString(i);
				}
				
				JSONObject solution = json.getJSONObject("Solution");
				JSONArray solutionwords = solution.getJSONArray("SolutionWords");
				String id = json.getString("Id");
				
				WordArray = new Word[solutionwords.length()];
				
				for(int i =0; i < solutionwords.length(); i++){
					JSONObject obj = solutionwords.getJSONObject(i);
					String Word = obj.optString("Word");
					String _dir = obj.optString("Direction");
					String _Column = obj.optString("Column");
					String _Row = obj.optString("Row");
					
					Word _word = new Word(Word);
					_word.Direction = Integer.parseInt(_dir);
					_word.Column = Integer.parseInt(_Column);
					_word.Row = Integer.parseInt(_Row);
					WordArray[i] = _word;
				}
				
			    _puzzle = new Puzzle();
				_puzzle.Characters = SingleLetters(outputstrings);
				_puzzle.WordsArray = WordArray;
				_puzzle.Date = GetDate();
				_puzzle.Id = id;
				_puzzle.width = outputstrings[0].length();
	 			datetext.setText("Date: " + _puzzle.Date);
				Puzzles.add(_puzzle);
				
				DrawPuzzle();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void DrawPuzzle(){
			PuzzleGrid = (GridView)findViewById(R.id.gridview1);
			PuzzleGrid.setNumColumns(_puzzle.width);
			PuzzleGrid.setAdapter(new TextViewAdapter(this,_puzzle.Characters,1,false));
			PuzzleGrid.setOnTouchListener(MyOnTouchListener);
		}
		
		public void NextClick(View v) {
			NextPuzzle();
		}
		
		public void NextPuzzle(){
			Pos++;
			Button prevbtn = (Button)findViewById(R.id.Previousbtn);
			if(DateModifier >= -1){
				prevbtn.setEnabled(true);
			}
			DateModifier--;
			LoadPuzzles();
		}
		
		
		public void PrevClick(View v) {
			PreviousPuzzle();
		}
		
		public void PreviousPuzzle(){
			Pos--;
			Button prevbtn = (Button)findViewById(R.id.Previousbtn);
			if(DateModifier >= -1){
				boolean OldPuzzle = false;
				int currentdatemod = DateModifier;
				DateModifier = 0;
				for(Puzzle puzzl : Puzzles){
					if(puzzl.Date.equals(GetDate()))
						OldPuzzle = true;
				}
				DateModifier = currentdatemod;
				if(!OldPuzzle){
					prevbtn.setEnabled(false);
				} else {
					DateModifier = 0;
					LoadPuzzles();
					prevbtn.setEnabled(false);
				}
			} else {
				DateModifier++;
				LoadPuzzles();
				boolean OldPuzzle = false;
				int currentdatemod = DateModifier;
				DateModifier = 0;
				for(Puzzle puzzl : Puzzles){
					if(puzzl.Date.equals(GetDate()))
						OldPuzzle = true;
				}
				DateModifier = currentdatemod;
	
				if(DateModifier >= -1 && !OldPuzzle)
					prevbtn.setEnabled(false);
			}
		}
		
		
		public void DelClick(View v) {
			Puzzles.remove(_puzzle);
			///Puzzles.removeAll(Puzzles);
			if(Pos == 0) {
				if(Puzzles.size() > 1) {
				NextPuzzle();
				} else {
					Toast.makeText(getApplicationContext(), "Can't delete only puzzle in list", Toast.LENGTH_SHORT).show();
				}
			} else {
				PreviousPuzzle();
			}
			Toast.makeText(getApplicationContext(), "Puzzle Deleted", Toast.LENGTH_SHORT).show();
		}
		
		public void BackClick(View v) {
			Intent nextScreen = new Intent(this,MainPuzzleClass.class);
			nextScreen.putExtra("Puzzle", PuzzleString);
			nextScreen.putExtra("PreviousSolutions", ArraytoString(Puzzles));
			nextScreen.putExtra("Scores", Scores);
			this.startActivity(nextScreen);
			this.finish();
		}

		public void ShowSolution(View v)
		{
			if(DateModifier != 0) {
				GenerateSolutions();
			} else {
				Toast.makeText(getBaseContext(), "Can't View Solution for today's puzzle", Toast.LENGTH_SHORT).show();
			}
		}
		
		public String SingleArraytoString(Puzzle puzzle){
 			JSONObject newobj = new JSONObject();
 			try {
				newobj.put("Width", puzzle.width);
				newobj.put("Id", puzzle.Id);
				newobj.put("Finish", puzzle.finished);
				newobj.put("Date", _puzzle.Date);
				newobj.put("TPL", true);
				JSONArray chars = new JSONArray();
				for(String s: puzzle.Characters){
					JSONObject obj = new JSONObject();
					obj.put("String", s);
					chars.put(obj);
				}
				newobj.put("Characters", chars);
				JSONArray words = new JSONArray();
				for(Word _word: puzzle.WordsArray){
					JSONObject word = new JSONObject();
					if(puzzle.Date.equals(GetTodayDate())) {
						word.put("Column", _word.Column);
						word.put("Row", _word.Row);
						word.put("Length", _word.Text.length());
						word.put("Direction", _word.Direction);
						if(_word.Direction != -1){
							word.put("Found", true);
						} else {
							word.put("Found", false);
						}
					} else {
						word.put("Column", -1);
						word.put("Row", -1);
						word.put("Length", _word.Text.length());
						word.put("Direction", -1);
						word.put("Found", _word.Found);
					}
	
					word.put("Text", _word.Text);
					words.put(word);
				}
				newobj.put("Words", words);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
 		return  newobj.toString();
 	}
		
		public void Play(View v){
			//Save Old Puzzle
			String CurrentPuzzle = PuzzleString; //Save Old Puzzle
			String PuzzletoUse = SingleArraytoString(_puzzle); //Convert Puzzle We want to Use
			List<Puzzle> newPuzzleArray = Puzzles;
			//newPuzzleArray.remove(Pos); //Remove Old Puzzle From Previous Saved
			String OldPuzzles = ArraytoString(newPuzzleArray); //Convert to String
			
			JSONObject json = null, json2 = null;
			try {
				json = new JSONObject(CurrentPuzzle);
				json2 = new JSONObject(PuzzletoUse);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
			
			if(!json.optString("Date").equals(json2.opt("Date"))){
				try {
					int currentdatemod = DateModifier;
					DateModifier = 0;
					boolean already = false;
					for(Puzzle puz : newPuzzleArray){
						if(puz.Date.equals(GetDate())){ //if there is previous one saved
							already = true;
						}
					}
					if(json.optString("Date").equals(GetDate()) && already == false) {
						OldPuzzles =  CurrentPuzzle + "#" + OldPuzzles; //Add Todays At the Front Puzzle
					}
					
					DateModifier = currentdatemod;
				
				} catch(Exception e){
					
				}
				
				
				//Load Appropriate puzzle
				Intent nextScreen = new Intent(this,MainPuzzleClass.class);
				nextScreen.putExtra("Puzzle", PuzzletoUse); 
				nextScreen.putExtra("PreviousSolutions", OldPuzzles);
				nextScreen.putExtra("Scores", Scores);
				this.startActivity(nextScreen);
				this.finish();
			} else {
				Toast.makeText(getApplicationContext(), "Can't replay puzzle", Toast.LENGTH_SHORT).show();
			}
		}
		
		
		public String[] SingleLetters(String[] text){
		
		    String Letters = "";
		 
		    for(int i = 0; i < text.length; i++){ //Make a huge string
		    	Letters += text[i];
		    }
		    
		    char [] Letterchar = Letters.toCharArray();
		    String [] SingleLetterArray = new String[Letterchar.length];
		    for(int i = 0; i < Letterchar.length; i++){ //Convert char[] to string[]
		    	SingleLetterArray[i] = "" + Letterchar[i];
		    }
		    return SingleLetterArray;
		}
		
		public void GenerateSolutions(){

			
			for(int i= 0; i < _puzzle.WordsArray.length; i++){
				Word obj = _puzzle.WordsArray[i];
				PuzzleGrid = (GridView)findViewById(R.id.gridview1);
				rowcount = (PuzzleGrid.getCount() / _puzzle.width); //because it starts at 0
				int [] _pos = Positions(obj.Direction,obj.Row,obj.Column,obj.Text.length());
	

				for(int j =0; j < _pos.length; j++){
					 Adapter adapt = PuzzleGrid.getAdapter();
					 TextView text = (TextView) adapt.getItem(_pos[j]);
					text.setTextColor(0xFFFF0000);
				}
				
			}
		}
		int rowcount = 0;
		
		public int[] Positions(int Direction, int row, int column, int length){
			int [] _Positions = new int[length];

			int correctionrow = (rowcount - (row + 1));
			
			_Positions[0] = (correctionrow * _puzzle.width) + column;
			
			for(int i = 1; i < length; i++){
				_Positions[i] = ReturnPosFromDirection(_Positions[i-1], Direction);
			}
			
			return _Positions;
		}

		public int ReturnPosFromDirection(int pos1, int Direction){
			switch(Direction){
				case 0:
					return (pos1 + _puzzle.width - 1);
				case 1:
					return (pos1 + _puzzle.width);
				case 2:
					return (pos1 + _puzzle.width + 1);
				case 3:
					return (pos1 - 1);
				case 4:
					return (pos1 + 1);
				case 5:
					return (pos1 - _puzzle.width - 1);
				case 6:
					return (pos1 - _puzzle.width);
				case 7:
					return (pos1 - _puzzle.width + 1);
			}
			return -1;
		}

		
}


