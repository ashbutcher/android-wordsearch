package com.example.angrywordsearch;


import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;


public class SendResponseData extends AsyncTask<String, Void, String> {
	
	@Override
	protected String doInBackground(String... urls) 
	{
		String responseData = "";
		if (urls.length > 0 || urls.length < 2)
		{
			responseData = postHttpData(urls[0], urls[1]);
		}
		return responseData;
	}
	
	@Override
	protected void onPostExecute(String responseData) 
	{
		listener.onSendCompleted(responseData);
	}
		
	private OnSendDataCompleted listener;	
	
	public SendResponseData(OnSendDataCompleted listener) 
	{
		this.listener=listener;
	}

	public String postHttpData(String url, String obj)
	{
		String collectedDataString = "";
	
		int statusCode = -1;
 		HttpClient client = new DefaultHttpClient();
 		HttpPost PostRequest = new HttpPost(url);
 		try
 		{
 			StringEntity jsonentity = new StringEntity(obj);
 			PostRequest.setEntity(jsonentity);
 			PostRequest.setHeader("Accept","application/json");
 			PostRequest.setHeader("Content-Type","application/json");

 			HttpResponse response = client.execute(PostRequest);
 			StatusLine statusLine = response.getStatusLine();
 			statusCode = statusLine.getStatusCode();
 			if(statusCode == 202)
 			{
 				Header [] Headers = response.getAllHeaders();
 				for(Header _headers : Headers)
 				{
 					if(_headers.getName().equals("X-SubmitResult")){
 						collectedDataString =  _headers.getValue();
 					}
 				}
 			}
 		}
		catch (Exception e)
		{
			System.out.println("Status Code Not 200: Error");
			System.out.println(statusCode);
		}		

		return collectedDataString;
	}
}
