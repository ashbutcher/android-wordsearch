package com.example.angrywordsearch;

import java.io.FileInputStream;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import com.example.angrywordsearch.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;


public class AngryWordsearch extends Activity{

	LoginHandler _loginHandler;
	String Username, Password = "";
	String GPS1, GPS2, GPS3, GPS4  = "", GPS5, GPS6 = "", GPS7 = "", GPS8 = ""; //GSP : General Purpose String
	enum States {REGISTER, USERDETAILS, CHNGPSSWRD, SCORES, PREVSOLS, PUZZLE, LOGIN};
	States State;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    setContentView(R.layout.login);
	    _loginHandler = LoginParent._loginHandler;
	    
	      if(loadFromInputFileStream()){ //If we are restoring a previous state
	    	  Intent nextScreen = null;
	    	  switch(State){
		    	  case REGISTER:
		  	    	nextScreen = new Intent(this,RegisterUserClass.class);
					nextScreen.putExtra("Username", GPS1);
					nextScreen.putExtra("Password", GPS2);
					nextScreen.putExtra("FirstName", GPS3);
					nextScreen.putExtra("LastName", GPS4);
	    		  break;
	    		  
		    	  case LOGIN:
			  	    	nextScreen = new Intent(this,LoginClass.class);
						nextScreen.putExtra("Username", GPS1);
						nextScreen.putExtra("Password", GPS2);
		    		  break;
	    		  
		    	  case PUZZLE:
		  	    	nextScreen = new Intent(this,MainPuzzleClass.class);
					_loginHandler.set_uname(GPS1);
					_loginHandler.set_password(GPS2);
					_loginHandler.set_RealName(GPS3);
					nextScreen.putExtra("Puzzle", GPS4);
					nextScreen.putExtra("PreviousSolutions", GPS5);
					nextScreen.putExtra("Scores", GPS6);
		    		  break;
	    	  
		    	  case USERDETAILS:
		  	    	nextScreen = new Intent(this,UserDetailsClass.class);
					_loginHandler.set_uname(GPS1);
					_loginHandler.set_password(GPS2);
					_loginHandler.set_RealName(GPS3);
					nextScreen.putExtra("Puzzle", GPS4);
					nextScreen.putExtra("PreviousSolutions", GPS5);
					nextScreen.putExtra("Scores", GPS6);
	    		  break;
	    		  
		    	  case CHNGPSSWRD:
			  	    	nextScreen = new Intent(this,ChangePasswordClass.class);
						_loginHandler.set_uname(GPS1);
						_loginHandler.set_password(GPS2);
						_loginHandler.set_RealName(GPS3);
						nextScreen.putExtra("Puzzle", GPS4);
						nextScreen.putExtra("FirstPassword", GPS5);
						nextScreen.putExtra("SecondPassword", GPS6);
						nextScreen.putExtra("PreviousSolutions", GPS7);
						nextScreen.putExtra("Scores", GPS8);
		    		  break;
		    		  
		    	  case PREVSOLS:
			  	    	nextScreen = new Intent(this,PreviousPuzzlesClass.class);
						_loginHandler.set_uname(GPS1);
						_loginHandler.set_password(GPS2);
						_loginHandler.set_RealName(GPS3);
						nextScreen.putExtra("Puzzle", GPS4);
						nextScreen.putExtra("PreviousSolutions", GPS5);
						nextScreen.putExtra("Scores", GPS6);
		    		  break;
		    		  
		    	  case SCORES:
			  	    	nextScreen = new Intent(this,ScoreClass.class);
						_loginHandler.set_uname(GPS1);
						_loginHandler.set_password(GPS2);
						_loginHandler.set_RealName(GPS3);
						nextScreen.putExtra("Puzzle", GPS4);
						nextScreen.putExtra("PreviousSolutions", GPS5);
						nextScreen.putExtra("Scores", GPS6);
		    		  break;
  	  }
	    	  
		    	this.startActivity(nextScreen);
				this.finish();
	      } else {
	    	  Intent nextScreen = new Intent(this,RegisterUserClass.class);
			  this.startActivity(nextScreen);
			  this.finish();
	      }
	 }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.angry_wordsearch, menu);
		return true;
	}
	

	private Boolean loadFromInputFileStream() {
		FileInputStream inputStream;
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			
				inputStream = openFileInput("end.xml");
				Document document = documentBuilder.parse(inputStream);
				loadData(document);
		    inputStream.close();
		    return true;
		} catch(Exception e){
			return false;
		}
	}
	
	private void loadData(Document xmlDocument) {
		NodeList nodeList =  xmlDocument.getElementsByTagName("State");
	      if(nodeList.getLength() > 0 && nodeList.getLength() <= 2)
	      {
	            Element LoginElement = (Element)nodeList.item(0);
	            NodeList hourNodes =  LoginElement.getElementsByTagName("PrevState");
	            if(hourNodes.getLength() > 0 && hourNodes.getLength() < 2)
	            {
	               Element currentElement = (Element)hourNodes.item(0);
	               String tempString = currentElement.getTextContent();
	               if(tempString.equals("Register")){
	            	   State = States.REGISTER;
	               } else if(tempString.equals("Puzzle")){
	            	   State = States.PUZZLE;
	               } else if(tempString.equals("UserDetails")){
	            	   State = States.USERDETAILS;
	               } else if(tempString.equals("ChangePassword")){
	            	   State = States.CHNGPSSWRD;
	               } else if(tempString.equals("PrevPuzzles")){
	            	   State = States.PREVSOLS;
	               } else if(tempString.equals("Scores")){
	            	   State = States.SCORES;
	               } else if(tempString.equals("Login")){
	            	   State = States.LOGIN;
	               }
	             
	            }
	            
	            NodeList UsernameElements =  LoginElement.getElementsByTagName("Username");
	            if(hourNodes.getLength() > 0 && hourNodes.getLength() < 2)
	            {
	               Element currentElement = (Element)UsernameElements.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS1 = tempString;
	            }
	            
	            NodeList MinutesNodes =  LoginElement.getElementsByTagName("Password");
	            if(MinutesNodes.getLength() > 0 && MinutesNodes.getLength() < 2)
	            {
	               Element currentElement = (Element)MinutesNodes.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS2 = tempString;
	            } 
	            
	            NodeList FirstNameNodes =  LoginElement.getElementsByTagName("FirstName");
	            if(FirstNameNodes.getLength() > 0 && FirstNameNodes.getLength() < 2)
	            {
	               Element currentElement = (Element)FirstNameNodes.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS3 = tempString;
	            } 
	            
	            NodeList RealNameNodes =  LoginElement.getElementsByTagName("RealName");
	            if(RealNameNodes.getLength() > 0 && RealNameNodes.getLength() < 2)
	            {
	               Element currentElement = (Element)RealNameNodes.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS3 = tempString;
	            } 
	            
	            NodeList LastNameNodes =  LoginElement.getElementsByTagName("LastName");
	            if(LastNameNodes.getLength() > 0 && LastNameNodes.getLength() < 2)
	            {
	               Element currentElement = (Element)LastNameNodes.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS4 = tempString;
	            } 
	            
	            NodeList PuzzleNodes =  LoginElement.getElementsByTagName("PuzzleData");
	            if(PuzzleNodes.getLength() > 0 && PuzzleNodes.getLength() < 2)
	            {
	               Element currentElement = (Element)PuzzleNodes.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS4 = tempString;
	            } 
	            
	            NodeList FirstPassword =  LoginElement.getElementsByTagName("FirstPassword");
	            if(FirstPassword.getLength() > 0 && FirstPassword.getLength() < 2)
	            {
	               Element currentElement = (Element)FirstPassword.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS5 = tempString;
	            } 
	            
	            NodeList SecondPassword =  LoginElement.getElementsByTagName("SecondPassword");
	            if(SecondPassword.getLength() > 0 && SecondPassword.getLength() < 2)
	            {
	               Element currentElement = (Element)SecondPassword.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS6 = tempString;
	            } 
	            
	            NodeList PrevPuzzleData =  LoginElement.getElementsByTagName("PrevPuzzleData");
	            if(PrevPuzzleData.getLength() > 0 && PrevPuzzleData.getLength() < 2)
	            {
	               Element currentElement = (Element)PrevPuzzleData.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS5 = tempString;
	            } 
	            
	            NodeList ScoresData =  LoginElement.getElementsByTagName("Scores");
	            if(ScoresData.getLength() > 0 && ScoresData.getLength() < 2)
	            {
	               Element currentElement = (Element)ScoresData.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS6 = tempString;
	            } 
	            
	            NodeList PrevPuzzle =  LoginElement.getElementsByTagName("PrevPuzzle");
	            if(PrevPuzzle.getLength() > 0 && PrevPuzzle.getLength() < 2)
	            {
	               Element currentElement = (Element)PrevPuzzle.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS7 = tempString;
	            } 
	            
	            NodeList PrevScoresData =  LoginElement.getElementsByTagName("PrevScores");
	            if(PrevScoresData.getLength() > 0 && PrevScoresData.getLength() < 2)
	            {
	               Element currentElement = (Element)PrevScoresData.item(0);
	               String tempString = currentElement.getTextContent();
	               GPS8 = tempString;
	            } 
	            
	      }

	}

}
