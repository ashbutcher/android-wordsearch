package com.example.angrywordsearch;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;


public class TextViewAdapter extends BaseAdapter {  
	 private Context context;  

	 String[] filenames;
	 TextView[] textview;
	Boolean ShowNumbers = false;
	 
	 // Gets the context so it can be used later  
	 public TextViewAdapter(Context c, String [] names, int Multiplier, Boolean number) {  
		 this.context = c;
		 filenames = names;
		 this.multiplier = Multiplier;
		 this.ShowNumbers = number;
		 textview = new TextView[names.length];
		 int count = names.length;
		 for(int i = 0; i < names.length; i++){
			 TextView tv = new TextView(c);
	            tv.setTextSize(textsize);
	            tv.setId(count--);
		        tv.setText(filenames[i]);
		        tv.setTextColor(0XFFFFFFFF);
		        textview[i] = tv;
		 }
	    }

	    public int getCount() {
	        return filenames.length;
	    }

	    public Object getItem(int position) {
	    	return textview[position];
	    }

	    public long getItemId(int position) {
	        return 0;
	    }
	    public int textsize = 12;
	    int Height = (int) (textsize * 2.25);
	    int Width = (int) (textsize * 1.25);
	    int multiplier = 0;

		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	         TextView tv;
	         
	        if (convertView == null) {
	            tv = textview[position];
	            tv.setLayoutParams(new GridView.LayoutParams(Width * multiplier, Height));
	        } else {
	            tv = textview[position];//(TextView) convertView;
	        }
	        tv.setTag(textview[position].getTag());
	        return tv;
	    }
	    
	  
	    
	}
