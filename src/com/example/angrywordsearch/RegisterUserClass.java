package com.example.angrywordsearch;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterUserClass extends Activity implements OnRetrieveDataCompleted {

	
	LoginHandler _loginHandler;
	String Password, FirstName, LastName, Username = "";
	EditText  unamebox, upassbox, ufirsbox, ulastbox;
	ProgressDialog pdialog;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
		    setContentView(R.layout.registeruser);
		    _loginHandler = LoginParent._loginHandler;
		    
		    Intent i= getIntent();
			  unamebox = (EditText) findViewById(R.id.RegUsernameBox);
			  upassbox = (EditText) findViewById(R.id.RegPasswordBox);
			  ufirsbox = (EditText) findViewById(R.id.RegFirstBox);
			  ulastbox = (EditText) findViewById(R.id.RegLastBox);
			  
			  unamebox.setNextFocusDownId(R.id.RegUsernameBox);
			  upassbox.setNextFocusDownId(R.id.RegPasswordBox);
			  ufirsbox.setNextFocusDownId(R.id.RegFirstBox);
			  ulastbox.setNextFocusDownId(R.id.RegLastBox);
			  
			  Username = i.getStringExtra("Username");
			  Password = i.getStringExtra("Password");
			  FirstName = i.getStringExtra("FirstName");
			  LastName = i.getStringExtra("LastName");
			  
			  unamebox.setText(Username);
			  upassbox.setText(Password);
			  ufirsbox.setText(FirstName);
			  ulastbox.setText(LastName);
			  
		    
		 }

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.angry_wordsearch, menu);
			return true;
		}
		
	    @Override
	    protected void onStop() {
		    super.onStop();
		    outputToFileStream("end.xml");
	    }
	    
	    @Override
	    protected void onPause() {
		    super.onPause();
		    outputToFileStream("end.xml");
	    }
		
		public void RegisterUser(View v)
		{
			  EditText unamebox = (EditText) findViewById(R.id.RegUsernameBox);
			  EditText upassbox = (EditText) findViewById(R.id.RegPasswordBox);
			  EditText ufirsbox = (EditText) findViewById(R.id.RegFirstBox);
			  EditText ulastbox = (EditText) findViewById(R.id.RegLastBox);
			  
			  Username = unamebox.getText().toString();
			  Password = upassbox.getText().toString();
			  FirstName = ufirsbox.getText().toString();
			  LastName = ulastbox.getText().toString();
			  
			  if(!Username.equals("") && !Password.equals("")&& !FirstName.equals("") && !LastName.equals("")) {
					String url = "http://08309.net.dcs.hull.ac.uk/api/admin/register?firstname=" + FirstName.trim() + "&Surname=" + LastName.trim() + "&username=" + unamebox.getText().toString().trim() + "&password=" + upassbox.getText().toString().trim();
					RetrieveResponseData retrieveResponseData = new RetrieveResponseData(this);
					retrieveResponseData.execute(url);
					pdialog=new ProgressDialog(this);
					pdialog.setCancelable(true);
					pdialog.setMessage("Loading ....");
					pdialog.show();
					} else {
						 Toast.makeText(getApplicationContext(), "Username and/or Password are blank", Toast.LENGTH_SHORT).show();
					}
			  

		}
		
		@Override
		public void onTaskCompleted(String responseData) {
			
				JSONObject json;
				try {
					pdialog.dismiss();
					json = new JSONObject(responseData);
					String containserrors = json.optString("Error");
					if(containserrors.equals(""))
					{
						Toast.makeText(getBaseContext(),"Registration Successful", Toast.LENGTH_SHORT).show();
						_loginHandler.LoginUser(Username, (FirstName + " " + LastName), Password);
						Intent nextScreen = new Intent(this,MainPuzzleClass.class);
						this.startActivity(nextScreen);
						this.finish();
					} else {
						 Toast.makeText(getApplicationContext(), containserrors, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		
		private Document generateClose() {
			
			  EditText unamebox = (EditText) findViewById(R.id.RegUsernameBox);
			  EditText upassbox = (EditText) findViewById(R.id.RegPasswordBox);
			  EditText ufirsbox = (EditText) findViewById(R.id.RegFirstBox);
			  EditText ulastbox = (EditText) findViewById(R.id.RegLastBox);
			  
			  Username = unamebox.getText().toString();
			  Password = upassbox.getText().toString();
			  FirstName = ufirsbox.getText().toString();
			  LastName = ulastbox.getText().toString();
			
			try {
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document document = documentBuilder.newDocument();
				
				Element rootElement = document.createElement("State");
				   document.appendChild(rootElement);

				Element State = document.createElement("PrevState");
				State.appendChild(document.createTextNode("Register"));
				
			   Element UsernameElement = document.createElement("Username");
			   UsernameElement.appendChild(document.createTextNode(Username));
			      
			   Element PasswordElement = document.createElement("Password");
			   PasswordElement.appendChild(document.createTextNode(Password));
			   
			   Element FirstNameElement = document.createElement("FirstName");
			   FirstNameElement.appendChild(document.createTextNode(FirstName));
			   
			   Element LastNameElement = document.createElement("LastName");
			   LastNameElement.appendChild(document.createTextNode(LastName));
			   
			   rootElement.appendChild(State);
			   rootElement.appendChild(UsernameElement);
			   rootElement.appendChild(PasswordElement);
			   rootElement.appendChild(FirstNameElement);
			   rootElement.appendChild(LastNameElement);
			   
		

				// refer to the lecture slides to add the XML elements to
				// the document
				return document;
			} catch(Exception e){}
			return null;
		}
		
		private String generateXMLString() {
			Document document = generateClose();
			if (document != null) {
				try {
					TransformerFactory factory = TransformerFactory.newInstance();
					 Transformer transformer = factory.newTransformer();
					 Properties outFormat = new Properties();
					 outFormat.setProperty(OutputKeys.INDENT, "yes");
					 outFormat.setProperty(OutputKeys.METHOD, "xml");
					 outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
					 outFormat.setProperty(OutputKeys.VERSION, "1.0");
					 outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
					 transformer.setOutputProperties(outFormat);
					 DOMSource domSource = new DOMSource(document.getDocumentElement());
					 OutputStream output = new ByteArrayOutputStream();
					 StreamResult streamResult = new StreamResult(output);
					 transformer.transform(domSource, streamResult);
					 String xmlString = output.toString();
					return xmlString;
				} catch(Exception e) {}
			}
			return null; 
		}

		public void outputToFileStream(String Name) {
		    FileOutputStream outputStream;
		    String xmlString = generateXMLString();

		    if (xmlString != null) {
		    	try {
				    outputStream = openFileOutput(Name, Context.MODE_PRIVATE);
				    outputStream.write(xmlString.getBytes());
					outputStream.close();
				} catch (Exception e) {}
		    }
		}
		
}
