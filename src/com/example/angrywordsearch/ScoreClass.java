package com.example.angrywordsearch;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ScoreClass extends Activity implements OnRetrieveDataCompleted {
	LoginHandler _loginHandler;
	Boolean SerialTransmissionIsntComplete = true;
	Boolean PrimaryScore = true;
	TextView scoretext;
	int DateModifier = 0;
	String CurrentDate;
	RetrieveResponseData retrieveResponseData;
	OnRetrieveDataCompleted listener;
	int id;
	static int ArraySize = 15;
	String [] ListArray  = new String[ArraySize];
	String PuzzleString = "", Puzzles = "";
	
	public String GetDate()  {
		Calendar cal=Calendar.getInstance();		
		cal.add(Calendar.DAY_OF_MONTH, DateModifier);
		Date currentdate = new Date(cal.getTimeInMillis());
		return currentdate.toString();
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    setContentView(R.layout.scoresheet);
	    _loginHandler = LoginParent._loginHandler;
    	listener = this;
	    Arrays.fill(ListArray, "N/A");
    	Intent i= getIntent();
    	if(i.getExtras() != null){
    		try {
			PuzzleString = i.getStringExtra("Puzzle");
    		}catch(Exception e){}
    		
	    	try{
	    		Puzzles = i.getStringExtra("PreviousSolutions");
	    	} catch(Exception e){}
    		
    		try {
			String temp = i.getStringExtra("Scores");
			String [] strings = temp.split("#");
			for(int j = 0; j < strings.length; j++){
				if(!strings[j].equals(""))
					ListArray[j] = strings[j];
			}

    		}catch(Exception e){}
    	}
    	
    	String url = "http://08309.net.dcs.hull.ac.uk/api/admin/score?username=" + _loginHandler.get_uname() + "&password=" + _loginHandler.get_password() + "&date=" + GetDate();
    	retrieveResponseData = new RetrieveResponseData(this);
    	retrieveResponseData.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, url);
    	
		scoretext = (TextView)findViewById(R.id.currentScoreText);
	  	Runnable runnable = new Runnable() {
	        public void run() {     	
		    	while(SerialTransmissionIsntComplete){
		    		try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {}
		    	}
		    	PrimaryScore = false;
				SerialTransmissionIsntComplete = true;

				for(int i = 0; i < ArraySize; i++){
				   	DateModifier--;
					if(ListArray[i].equals("N/A")){
					   	id = i;
					   	CurrentDate = GetDate();
				    	String url = "http://08309.net.dcs.hull.ac.uk/api/admin/score?username=" + _loginHandler.get_uname() + "&password=" + _loginHandler.get_password() + "&date=" + GetDate();
				    	RetrieveResponseData retrieveResponseData2 = new RetrieveResponseData(listener);
				    	retrieveResponseData2.execute(url);
				    	
				    	while(SerialTransmissionIsntComplete){
				    		try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {}
				    	}
				    	//update data
						SerialTransmissionIsntComplete = true;
					}	
				}
	        }
	  };
	  	Thread mythread = new Thread(runnable);
	  	mythread.start();	   
	 }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.angry_wordsearch, menu);
		return true;
	}
	
	public void BackClick(View v) {
		Intent nextScreen = new Intent(this,UserDetailsClass.class);
		nextScreen.putExtra("Puzzle", PuzzleString);
		   String tempstring = "";
		   for(int i =0; i < ArraySize; i++){
			   tempstring += ListArray[i] + "#";
		   }
		nextScreen.putExtra("PreviousSolutions",Puzzles);
		nextScreen.putExtra("Scores", tempstring);
		this.startActivity(nextScreen);
		this.finish();
		
	}

	
	  protected void onStop() {
		    super.onStop();
		    outputToFileStream("end.xml");
	    }
	    
	    @Override
	    protected void onPause() {
		    super.onPause();
		   this.finish();
	    }

		private Document generateClose() {
			try {
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document document = documentBuilder.newDocument();
				
				Element rootElement = document.createElement("State");
				document.appendChild(rootElement);

				Element State = document.createElement("PrevState");
				State.appendChild(document.createTextNode("Scores"));
				
			   Element UsernameElement = document.createElement("Username");
			   UsernameElement.appendChild(document.createTextNode(_loginHandler.get_uname()));
			      
			   Element PasswordElement = document.createElement("Password");
			   PasswordElement.appendChild(document.createTextNode(_loginHandler.get_password()));
			   
			   Element FirstNameElement = document.createElement("RealName");
			   FirstNameElement.appendChild(document.createTextNode(_loginHandler.get_RealName()));
			   
			   Element LastNameElement = document.createElement("PuzzleData");
			   LastNameElement.appendChild(document.createTextNode(PuzzleString));
			   
			   String tempstring = "";
			   for(int i =0; i < ArraySize; i++){
				   tempstring += ListArray[i] + "#";
			   }
			   
			   Element ScoresElement = document.createElement("Scores");
			   ScoresElement.appendChild(document.createTextNode(tempstring));
			   
			   Element PrevElement = document.createElement("PrevPuzzleData");
			   PrevElement.appendChild(document.createTextNode(Puzzles));
			   
			   rootElement.appendChild(State);
			   rootElement.appendChild(UsernameElement);
			   rootElement.appendChild(PasswordElement);
			   rootElement.appendChild(FirstNameElement);
			   rootElement.appendChild(LastNameElement);
			   rootElement.appendChild(ScoresElement);
			   rootElement.appendChild(PrevElement);
			   
		

				// refer to the lecture slides to add the XML elements to
				// the document
				return document;
			} catch(Exception e){}
			return null;
		}
		
		private String generateXMLString() {
			Document document = generateClose();
			if (document != null) {
				try {
					TransformerFactory factory = TransformerFactory.newInstance();
					 Transformer transformer = factory.newTransformer();
					 Properties outFormat = new Properties();
					 outFormat.setProperty(OutputKeys.INDENT, "yes");
					 outFormat.setProperty(OutputKeys.METHOD, "xml");
					 outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
					 outFormat.setProperty(OutputKeys.VERSION, "1.0");
					 outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
					 transformer.setOutputProperties(outFormat);
					 DOMSource domSource = new DOMSource(document.getDocumentElement());
					 OutputStream output = new ByteArrayOutputStream();
					 StreamResult streamResult = new StreamResult(output);
					 transformer.transform(domSource, streamResult);
					 String xmlString = output.toString();
					return xmlString;
				} catch(Exception e) {}
			}
			return null; 
		}

		public void outputToFileStream(String Name) {
		    FileOutputStream outputStream;
		    String xmlString = generateXMLString();

		    if (xmlString != null) {
		    	try {
				    outputStream = openFileOutput(Name, Context.MODE_PRIVATE);
				    outputStream.write(xmlString.getBytes());
					outputStream.close();
				} catch (Exception e) {}
		    }
		}
		
	
	
	@Override
	public void onTaskCompleted(String responseData) {
		JSONObject json;
		try {
			json = new JSONObject(responseData);
			String _Score = json.optString("Result");
			if(PrimaryScore)
			{
				scoretext.setText(_Score);
			} else {
				ListArray[id] = CurrentDate + " " + _Score;
			}
			SerialTransmissionIsntComplete = false;

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void Click(View v){
		ListView listV = (ListView) findViewById(R.id.PreviousScores);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, ListArray);
		listV.setAdapter(adapter);
	}

}
