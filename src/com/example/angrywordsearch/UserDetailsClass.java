package com.example.angrywordsearch;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;



public class UserDetailsClass extends Activity implements OnRetrieveDataCompleted {
	LoginHandler _loginHandler;
	TextView password;
	String PuzzleString = "", Scores ="", Puzzles ="";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    setContentView(R.layout.userdetails);
	    _loginHandler = LoginParent._loginHandler;
	    
	    Intent i= getIntent();

		    	if(i.getExtras() != null){
			    	try {
						PuzzleString = i.getStringExtra("Puzzle");
					} catch (Exception e){}
			    	
			    	try{
			    		Puzzles = i.getStringExtra("PreviousSolutions");
			    	} catch(Exception e){}
					
			    	try {
				    	Scores = i.getStringExtra("Scores");
					} catch (Exception e){}
		    	}
	    
	    TextView realname = (TextView)findViewById(R.id.RealnameText);
	    TextView username = (TextView)findViewById(R.id.UsernameText);
	    password = (TextView)findViewById(R.id.PasswordText);
	    realname.setText(_loginHandler.get_RealName());
	    username.setText(_loginHandler.get_uname());
	    password.setText(_loginHandler.get_password());
	    
	    
	    Switch toggle = (Switch) findViewById(R.id.HidePasswordSwitch);
	    toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
	        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	            if (isChecked) {
	            	password.setInputType(InputType.TYPE_CLASS_TEXT);
	            } else {
	            	password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
	            }
	        }
	    });
	 }

	
    protected void onStop() {
	    super.onStop();
	    outputToFileStream("end.xml");
    }
    
    @Override
    protected void onPause() {
	    super.onPause();
	    outputToFileStream("end.xml");
    }

	private Document generateClose() {
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();
			
			Element rootElement = document.createElement("State");
			   document.appendChild(rootElement);

			Element State = document.createElement("PrevState");
			State.appendChild(document.createTextNode("UserDetails"));
			
		   Element UsernameElement = document.createElement("Username");
		   UsernameElement.appendChild(document.createTextNode(_loginHandler.get_uname()));
		      
		   Element PasswordElement = document.createElement("Password");
		   PasswordElement.appendChild(document.createTextNode(_loginHandler.get_password()));
		   
		   Element FirstNameElement = document.createElement("RealName");
		   FirstNameElement.appendChild(document.createTextNode(_loginHandler.get_RealName()));
		   
		   Element LastNameElement = document.createElement("PuzzleData");
		   LastNameElement.appendChild(document.createTextNode(PuzzleString));
		   
		   Element PrevElement = document.createElement("PrevPuzzleData");
		   PrevElement.appendChild(document.createTextNode(Puzzles));
		   
		   Element ScoresElement = document.createElement("Scores");
		   ScoresElement.appendChild(document.createTextNode(Scores));
		   
		   rootElement.appendChild(State);
		   rootElement.appendChild(UsernameElement);
		   rootElement.appendChild(PasswordElement);
		   rootElement.appendChild(FirstNameElement);
		   rootElement.appendChild(LastNameElement);
		   rootElement.appendChild(ScoresElement);
		   rootElement.appendChild(PrevElement);
		   
	

			// refer to the lecture slides to add the XML elements to
			// the document
			return document;
		} catch(Exception e){}
		return null;
	}
	
	private String generateXMLString() {
		Document document = generateClose();
		if (document != null) {
			try {
				TransformerFactory factory = TransformerFactory.newInstance();
				 Transformer transformer = factory.newTransformer();
				 Properties outFormat = new Properties();
				 outFormat.setProperty(OutputKeys.INDENT, "yes");
				 outFormat.setProperty(OutputKeys.METHOD, "xml");
				 outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
				 outFormat.setProperty(OutputKeys.VERSION, "1.0");
				 outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
				 transformer.setOutputProperties(outFormat);
				 DOMSource domSource = new DOMSource(document.getDocumentElement());
				 OutputStream output = new ByteArrayOutputStream();
				 StreamResult streamResult = new StreamResult(output);
				 transformer.transform(domSource, streamResult);
				 String xmlString = output.toString();
				return xmlString;
			} catch(Exception e) {}
		}
		return null; 
	}

	public void outputToFileStream(String Name) {
	    FileOutputStream outputStream;
	    String xmlString = generateXMLString();

	    if (xmlString != null) {
	    	try {
			    outputStream = openFileOutput(Name, Context.MODE_PRIVATE);
			    outputStream.write(xmlString.getBytes());
				outputStream.close();
			} catch (Exception e) {}
	    }
	}
    
	public void ChangePasswordClick(View v) {
		Intent nextScreen = new Intent(this,ChangePasswordClass.class);
		nextScreen.putExtra("Puzzle", PuzzleString);
		this.startActivity(nextScreen);
		this.finish();

	}
	
	public void UserLogout(View v) {
		Intent nextScreen = new Intent(this,LoginClass.class);
		PuzzleString = "";
		Scores = "";
		Puzzles = "";
		this.startActivity(nextScreen);
		this.finish();

	}
	
	boolean PassShown = true;
	
	
	public void BackClick(View v) {
		Intent nextScreen = new Intent(this,MainPuzzleClass.class);
		nextScreen.putExtra("Puzzle", PuzzleString);
		nextScreen.putExtra("PreviousSolutions", Puzzles);
		nextScreen.putExtra("Scores", Scores);
		this.startActivity(nextScreen);
		this.finish();
	}
	
	public void ScoreClick(View v) {
		Intent nextScreen = new Intent(this,ScoreClass.class);
		nextScreen.putExtra("Puzzle", PuzzleString);
		nextScreen.putExtra("PreviousSolutions", Puzzles);
		nextScreen.putExtra("Scores", Scores);
		this.startActivity(nextScreen);
		this.finish();
	}

	public void UnReg(View v) {
		String url = "http://08309.net.dcs.hull.ac.uk/api/admin/unregister?username=" + _loginHandler.get_uname() + "&password="+ _loginHandler.get_password();
		RetrieveResponseData retrieveResponseData = new RetrieveResponseData(this);
		retrieveResponseData.execute(url);
	}
	
	@Override
	public void onTaskCompleted(String responseData) {
		JSONObject json;
		try {
			json = new JSONObject(responseData);
			String containserrors = json.optString("Error");
			if(containserrors.equals(""))
			{
				_loginHandler.UnRegister();
				Toast.makeText(getBaseContext(),"User Successfully Un-Registered", Toast.LENGTH_SHORT).show();
				Intent nextScreen = new Intent(this,RegisterUserClass.class);
				this.startActivity(nextScreen);
				PuzzleString = "";
				Scores = "";
				Puzzles = "";
				this.finish();
			} else {
				Toast.makeText(getBaseContext(),"Couldn't Un-Register User", Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
