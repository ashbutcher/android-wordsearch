package com.example.angrywordsearch;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.os.AsyncTask;

public class RetrieveResponseData extends AsyncTask<String, Void, String> {
	
	@Override
	protected String doInBackground(String... urls) 
	{
		String responseData = "";
		if (urls.length > 0)
		{
			responseData = getHttpData(urls[0]);
		}
		return responseData;
	}
	
	@Override
	protected void onPostExecute(String responseData) 
	{
		try {
			JSONObject newobj = new JSONObject(responseData);
		} catch(Exception e){
			responseData = "{\"Error\":\"Could not connect to server!\"}";
		}
		listener.onTaskCompleted(responseData);
	}
		
	private OnRetrieveDataCompleted listener;	
	public RetrieveResponseData(OnRetrieveDataCompleted listener) 
	{
		this.listener=listener;
	}

	public String getHttpData(String url)
	{
		int statusCode = -1;
		StringBuilder stringBuilder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		try
		{
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			statusCode = statusLine.getStatusCode();
			if(statusCode == 200)
			{
				System.out.println("Status Code 200: OK");
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null)
				{
					stringBuilder.append(line);
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Status Code Not 200: Error");
			System.out.println(statusCode);
		}		
		String collectedDataString = stringBuilder.toString();
		return collectedDataString;
	}
}
